<!DOCTYPE html>
<html lang="en">
<head>
    <title>add menu</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Menu</h2>
    <form role="form" action="store_menu.php" method="post">
        <div class="form-group">
            <label>Menu:</label>
            <input type="text" name="menu" class="form-control" value="" placeholder="Enter Menu">
        </div>
        <div class="form-group">
            <label>Link:</label>
            <input type="text" name="link" class="form-control" value="" placeholder="Enter Link">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
