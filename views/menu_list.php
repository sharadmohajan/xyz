<?php
session_start();
include_once ('../vendor/autoload.php');
use App\Admin\Auth;
use App\Utility\Utility;
use App\Message\Message;
$auth = new Auth();
$totalItem=$auth->allmenu();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <style>
        tr:nth-child(even) {background: #ccc}
        tr:nth-child(odd) {background: #ccc}
    </style>
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome_admin.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
           <li><a href="add_menu.php">Add Menu</a>
            <li><a href="all_doctor.php">All Doctor</a></li>
            <li><a href="all_released.php">All Released</a></li>
            <li><a href="room_list.php">All Room</a></li>
            <li><a href="dept_list.php">All Dept</a></li>
    </div>
    </form></li>

    </ul>
    </div>
</nav>
<div class="container">
    <h2 class="jumbotron" align="center">All Menu</h2>
    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>


    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <div>
            <tr>
                <th>SL#</th>
                <th>ID</th
                    <th> </th>
                <th>Menu</th>
                <th>Action</th>
            </tr>
                </div>
            </thead>
            <tbody>
            <?php
            $sl=0;
                foreach ($totalItem as $item){
                    $sl++?>
                    <tr>
                        <td><?php echo $sl ?></td>
                        <td><?php echo $item->id ?></td>
                        <td><?php echo $item->menus ?></td>
                        <td>

                                <a href="edit_menu.php?menu_id=<?php echo $item->id ?>" class="btn btn-primary" role="button">Edit Menu</a>
                            <a href="delete_menu.php?menu_id=<?php echo $item->id ?>" class="btn btn-danger" role="button">Delete Menu</a>

                        </td>
                    </tr>
                <?php }?>

            </tbody>
        </table>

    </div>
</div>
<footer class="text-center" style="margin-top: 215px">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>

