<?php
require_once('../vendor/mpdf/mpdf/mpdf.php');
include_once('../vendor/autoload.php');
use App\Patient\Patient;
use App\Doctor\Doctor;
use App\Utility\Utility;
$patient= new Patient();
$doctor= new Doctor();
$singlePatient=$patient->prepare($_GET)->view();
//$Doctor_Id[]=array();
$Doctor_Id['doctor_id']=$singlePatient->doctor_id;
//var_dump($Doctor_Id);
//die();
$singlePrescription=$patient->prepare($_GET)->view_prescription();
$singleDoctor=$doctor->prepare($Doctor_Id)->view();
$trs="";
$trs.="<ul>";
$trs.="<li>ID: <?php echo $singlePatient->patient_id ?></li>";
$trs.="<li>Patient Name: <?php echo $singlePatient->patient_name ?></li>";
$trs.="<li>Doctor Name: <?php echo $singleDoctor->doctor_name ?></li>";
$trs.="<li>Disease: <?php echo $singlePatient->disease ?></li>";
$trs.="<li>Gender: <?php echo $singlePatient->gender ?></li>";
$trs.="<li>Age: <?php echo $singlePatient->age ?></li>";
$trs.="<li>Prescription: <?php echo $singlePrescription->drugs ?></li>";
$trs.="</ul>";

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($trs);

// Output a PDF file directly to the browser
$mpdf->Output('prescription.pdf','D');