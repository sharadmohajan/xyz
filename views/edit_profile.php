<?php
include_once('../vendor/autoload.php');
use App\Receptionist\Receptionist;
use App\Utility\Utility;

$receptionist= new Receptionist();
$singleItem=$receptionist->prepare($_GET)->view();
//Utility::dd($singleItem);


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Doctor Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--<style>
            tr:nth-child(even) {background: #ccc}
            tr:nth-child(odd) {background: #ccc}
        </style>-->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="add_doctor.php">Add Doctor</a></li>
            <li><a href="add_patient.php">Add Patient</a></li>
            <li><a href="patient_info.php">Doctors</a></li>
            <li><a href="patient_info.php">Patient Info</a></li>
            <li><a href="released_patient.php">Released Patient</a></li>
            <li><a href="view_profile.php?email=<?php echo $singleItem->email?>">view Profile</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <h2 class="jumbotron" align="center">Edit Profile</h2>
    <div class="jumbotron" style="padding-right: 150px;padding-left: 150px">
    <form role="form" action="update_profile.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <input type="hidden" name="receptionist_id"  value="<?php echo $singleItem->receptionist_id?>">
            <label>Name:</label>
            <input type="text" name="receptionist_name" class="form-control" value="<?php echo $singleItem->full_name?>">
        </div>
        <div class="form-group">
            <label>Age:</label>
            <input type="text" class="form-control"name="age" value="<?php echo $singleItem->age?>">
        </div>
        <div class="form-group">
            <label>Mobile No:</label>
            <input type="text" class="form-control"name="mobile" value="<?php echo $singleItem->mobile?>">
        </div>

        <div class="form-group">
            <label>Email Address:</label>
            <input type="email" class="form-control"name="email" value="<?php echo $singleItem->email?>">
        </div>
        <div class="form-group">
            <label>Password:</label>
            <input type="password" class="form-control"name="password" value="<?php echo $singleItem->password?>">
        </div>

        <div class="form-group">
            <label>Shift:</label>
            <input type="text" class="form-control"name="shift" value="<?php echo $singleItem->shift?>">
        </div>
        <div class="form-group">
            <label>Update profile picture:</label>
            <input type="file" class="form-control" id="pwd" name="image">
            <img src="../Resources/Images/<?php echo $singleItem->image?>" alt="image" height="50px" width="50px">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
        </div>
</div>
<footer class="text-center" style="margin-top: 180px">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>