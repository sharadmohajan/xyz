<?php
session_start();
include_once ('../../vendor/autoload.php');
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;
use App\Admin\Auth;


$auth= new Auth();
$status=$auth->logout();
if($status){
    return Utility::redirect('../../indexold.php');
}


