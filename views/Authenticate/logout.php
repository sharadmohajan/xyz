<?php
session_start();
include_once ('../../vendor/autoload.php');
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;
use App\User\Auth;


$auth= new Auth();
$status=$auth->logout();
if($status){
    return Utility::redirect('../../index.php');
}


