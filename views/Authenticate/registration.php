<?php
include_once ('../../vendor/autoload.php');
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;
use App\User\Auth;


$auth= new Auth();
$status=$auth->prepare($_POST)->is_exist();
if($status){
    Message::message("<div class=\"alert alert-danger\">
  <strong>Taken!</strong> Email already taken...
</div>");
    Utility::redirect('../../index.php');

}

else{
    $obj= new User();
    $obj->prepare($_POST)->store();
}



