<?php
session_start();
include_once ('../vendor/autoload.php');
use App\User\User;
use App\Utility\Utility;
use App\Message\Message;
$user = new User();
$totalItem=$user->count();
if(array_key_exists('itemPerPage',$_SESSION)) {
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];
    }

}
else{
    $_SESSION['itemPerPage']=5;
}
$itemPerPage= $_SESSION['itemPerPage'];


$noOfPage=ceil($totalItem/$itemPerPage);
//Utility::d($noOfPage);
$pagination="";
if(array_key_exists('pageNo',$_GET)){
    $pageNo= $_GET['pageNo'];
}else {
    $pageNo = 1;
}
for($i=1;$i<=$noOfPage;$i++){
    $active=($i==$pageNo)?"active":"";
    $pagination.="<li class='$active'><a href='receptionist_info.php?pageNo=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNo-1);
$allemail = $user->paginator($pageStartFrom, $itemPerPage);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <style>
        tr:nth-child(even) {background: #ccc}
        tr:nth-child(odd) {background: #ccc}
    </style>
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome_admin.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="all_doctor.php">All Doctor</a></li>
            <li><a href="all_released.php">All Released</a></li>
            <li><a href="room_list.php">All Room</a></li>
            <li><a href="dept_list.php">All Dept</a></li>
            <li> <form role="form" action="receptionist_info.php" style="margin-top: 13px;padding-left: 40px" >
                    <div class="form-group">
                        <select class="form-control" id="sel1" name="itemPerPage" onchange="this.form.submit()">
                            <option <?php if($itemPerPage==5) echo "selected"?>>5</option>
                            <option <?php if($itemPerPage==10) echo "selected"?>>10</option>
                            <option <?php if($itemPerPage==15) echo "selected"?>>15</option>
                            <option <?php if($itemPerPage==20) echo "selected"?>>20</option>
                            <option <?php if($itemPerPage==25) echo "selected"?>>25</option>
                        </select>
                        <!-- <button type="submit">GO!</button>-->

                    </div>
                </form></li>

        </ul>
    </div>
</nav>
<div class="container">
    <h2 class="jumbotron" align="center">All Receptionist</h2>
    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th
                    <th> </th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            if(isset($allemail))
                foreach ($allemail as $email){
                    $sl++?>
                    <tr>
                        <td><?php echo $sl+$pageStartFrom ?></td>
                        <td><?php echo $email->receptionist_id ?></td>
                        <td><?php echo $email->first_name." ".$email->last_name ?></td>
                        <td><?php echo $email->email ?></td>
                        <td>
                            <?php if($email->is_block !=1): ?>
                            <a href="block.php?id=<?php echo $email->receptionist_id ?>" class="btn btn-danger" role="button">Block</a>
                             <?php endif ?>
                            <?php if($email->is_block ==1): ?>
                            <a href="unblock.php?id=<?php echo $email->receptionist_id ?>" class="btn btn-primary" role="button">Unblock</a>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php }?>

            </tbody>
        </table>
        <center>
        <ul class="pagination">
            <?php if($pageNo>1)
            {
                $prev=$pageNo-1;
                echo  "<li ><a href = 'receptionist_info.php?pageNo=$prev'>Prev</a ></li >";
            }
            ?>
            <?php echo $pagination ?>
            <?php if($pageNo<$noOfPage)
            {
                $next=$pageNo+1;
                echo "<li ><a href = 'receptionist_info.php?pageNo=$next'>Next</a ></li >";
            }
            ?>
        </ul>
            </center>
    </div>
</div>
<footer class="text-center" style="margin-top: 215px">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>

