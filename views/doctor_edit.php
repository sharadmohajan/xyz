<?php
include_once('../vendor/autoload.php');
use App\Doctor\Doctor;
use App\Utility\Utility;

$doctor= new Doctor();
$singleItem=$doctor->prepare($_GET)->view();
//Utility::dd($singleItem);
$allItem=$doctor->deptList();

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Doctor Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--<style>
                        tr:nth-child(even) {background: #ccc}
                        tr:nth-child(odd) {background: #ccc}
                    </style>-->
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../Resources/bootstrap/js/bootstrap.js">

    <link href="../Resources/startbootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../Resources/startbootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../Resources/startbootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="welcome.php"><img src="../Resources/welcome/img/logo.PNG"width="100" height="30"></a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="add_patient.php">Add Patient</a></li>
            <li><a href="patient_info.php">Doctor Info</a></li>
            <li><a href="patient_info.php">Patient Info</a></li>
            <li><a href="released_patient.php">Released Patient</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <h2 class="jumbotron" align="center" style="padding-top: 10px;padding-bottom: 10px;color: #FFFFFF;background-color: #11866f">Edit Doctor</h2>
    <div class="jumbotron" style=" background-color:#11866f;padding-right: 150px;padding-left: 150px;color: #FFFFFF">
    <form role="form" action="doctor_update.php" method="post">
        <div class="form-group">
            <label>Doctor Id:</label>
            <input type="hidden" name="doctor_id"  value="<?php echo $singleItem->doctor_id?>">
            <input type="text" name="doctor_id" class="form-control" value="<?php echo $singleItem->doctor_id?>">
        </div>
        <div class="form-group">
            <label>Doctor Name:</label>
            <input type="text" class="form-control"name="doctor_name" value="<?php echo $singleItem->doctor_name?>">
        </div>
        <div class="form-group">
            <label>Mobile No:</label>
            <input type="text" class="form-control"name="mobile" value="<?php echo $singleItem->mobile?>">
        </div>

        <div class="form-group">
            <label>Email Address:</label>
            <input type="email" class="form-control"name="email" value="<?php echo $singleItem->email?>">
        </div>

        <div class="form-group">
            <label for="sel1">Edit Department:</label>
            <select class="form-control" id="sel1" name="dept_id">
                <?php foreach($allItem as $item){?>
                    <option value="<?php echo $item['dept_id']?>"<?php if($singleItem->dept_id ==$item['dept_id'])echo "selected";?>><?php echo $item['dept_name']?></option>
                <?php } ?>
            </select>
        </div>


        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>
</div>
<footer class="text-center" style="margin-top: 180px">
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; PHP HUNTERS 2016
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>