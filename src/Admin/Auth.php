<?php

namespace App\Admin;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
class Auth extends DB{


    public $email="";
    public $password="";
    public $menu_id="";
    public $menu="";
    public $dept_name="";
    public $dept_id="";
    public $room_id="";
    public $room_no="";
    public $link="";

    public function __construct()
    {
        parent::__construct();
    }
    public function prepare($data=Array())
    {
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('menu_id', $data)) {
            $this->menu_id = $data['menu_id'];
        }
        if (array_key_exists('menu', $data)) {
            $this->menu = $data['menu'];
        }
        if (array_key_exists('dept_name', $data)) {
            $this->dept_name = $data['dept_name'];
        }
        if (array_key_exists('dept_id', $data)) {
            $this->dept_id = $data['dept_id'];
        }
        if (array_key_exists('room_id', $data)) {
            $this->room_id = $data['room_id'];

        }
        if (array_key_exists('room_no', $data)) {
            $this->room_no = $data['room_no'];
        }
        if (array_key_exists('link', $data)) {
            $this->link = $data['link'];
        }
        return $this;
    }

   /* public function is_exist(){
        $query="SELECT * FROM `receptionist` WHERE `email`='".$this->email."'";
        $result= mysqli_query($this->conn,$query);
        $row=mysqli_num_rows($result);
          if($row>0){
              return TRUE;
          }
            else{
                return FALSE;
            }

    }*/


    public function is_registered(){
        $query="SELECT * FROM `admin` WHERE `email`= '".$this->email."' AND `password`='".$this->password."'";
        $result= mysqli_query($this->conn,$query);
        $row=mysqli_num_rows($result);
        if($row>0){
            return TRUE;
        }
        else{
            return FALSE;
        }
}
    public function logout()
    {
        if ((array_key_exists('user_email', $_SESSION)) && (!empty($_SESSION['user_email']))) {
            $_SESSION['user_email'] = "";
            return TRUE;
        }

    }
    public function is_loggedin()
    {
        if ((array_key_exists('user_email', $_SESSION)) && (!empty($_SESSION['user_email']))) {
            return TRUE;
        }

    }
    public function store(){
        $query="INSERT INTO `hms`.`menu` (`menus`,`link`) VALUES ('".$this->menu."','".$this->link."')";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You are successfully stored.
</div>");
            Utility::redirect('menu_list.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Menu has not been stored successfully.
</div>");
            Utility::redirect('menu_list.php');

        }

    }
    public function view(){
        $query="SELECT * FROM `menu` WHERE `id` =".$this->menu_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `hms`.`menu` SET `menus` = '".$this->menu."',`link` = '".$this->link."' WHERE `menu`.`id` = ".$this->menu_id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('menu_list.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('menu_list.php');

        }

    }
    public function delete()
    {
        $query = "DELETE FROM `hms`.`menu` WHERE `menu`.`id` =" . $this->menu_id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect('menu_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect('menu_list.php');


        }
    }

    public  function menuList()
    {
        //$_allItem=array();
        $query= "SELECT * FROM `menu`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
    public  function allmenu()
    {
        //$_allItem=array();
        $query= "SELECT * FROM `menu`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
    public  function alldept()
    {
        //$_allItem=array();
        $query= "SELECT * FROM `department`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }

    public function store_dept(){
        $query="INSERT INTO `hms`.`department` (`dept_name`) VALUES ('".$this->dept_name."')";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You are successfully stored.
</div>");
            Utility::redirect('dept_list.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Dept has not been stored successfully.
</div>");
            Utility::redirect('dept_list.php');

        }

    }
    public function view_dept(){
        $query="SELECT * FROM `department` WHERE `dept_id` =".$this->dept_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }
    public function update_dept(){
        $query="UPDATE `hms`.`department` SET `dept_name` = '".$this->dept_name."' WHERE `department`.`dept_id` = ".$this->dept_id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Dept has been updated successfully.
</div>");
            Utility::redirect('dept_list.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('dept_list.php');

        }

    }
    public  function allroom()
    {
        //$_allItem=array();
        $query= "SELECT * FROM `room`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }

    public function store_room(){
        $query="INSERT INTO `hms`.`room` (`room_no`) VALUES ('".$this->room_no."')";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You are successfully stored.
</div>");
            Utility::redirect('room_list.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> room has not been stored successfully.
</div>");
            Utility::redirect('room_list.php');

        }

    }
    public function view_room(){
        $query="SELECT * FROM `room` WHERE `room_id` =".$this->room_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }
    public function update_room(){
        $query="UPDATE `hms`.`room` SET `room_no` = '".$this->room_no."' WHERE `room`.`room_id` = ".$this->room_id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Room has been updated successfully.
</div>");
            Utility::redirect('room_list.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('room_list.php');

        }

    }
    public  function doctor_list()
    {
        $_allItem=array();
        $query= "SELECT p.doctor_name,d.dept_name FROM doctor AS p INNER JOIN department AS d ON p.dept_id=d.dept_id WHERE d.dept_id=".$this->dept_id;
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
    public function countPatient()
    {
        $query = "SELECT COUNT(*) AS total_inpatient FROM patient WHERE is_active Is NULL";
        $result = mysqli_query($this->conn, $query);
        $total_patient = mysqli_fetch_assoc($result);
        return $total_patient;
    }
    public function countReleased()
    {
        $query = "SELECT COUNT(*) AS total_released FROM patient WHERE is_active Is NOT NULL";
        $result = mysqli_query($this->conn, $query);
        $var = mysqli_fetch_assoc($result);
        return $var;
    }
    public function countDoctor()
    {
        $query = "SELECT COUNT(*) AS total_doctor FROM doctor";
        $result = mysqli_query($this->conn, $query);
        $var = mysqli_fetch_assoc($result);
        return $var;
    }
    public function countDept()
    {
        $query = "SELECT COUNT(*) AS total_dept FROM department";
        $result = mysqli_query($this->conn, $query);
        $var = mysqli_fetch_assoc($result);
        return $var;
    }
    public function countRoom()
    {
        $query = "SELECT COUNT(*) AS total_room FROM room";
        $result = mysqli_query($this->conn, $query);
        $var = mysqli_fetch_assoc($result);
        return $var;
    }
}

