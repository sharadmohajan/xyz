<?php

namespace App\Patient;
//session_start();
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
class Patient extends DB{
    public $patient_id;
    public $patient_name="";
    public $mobile="";
    public $disease="";
    public $room_id="";
    public $seat="";
    public $doctor_id="";
    public $entry_date="";
    public $gender="";
    public $age="";
    public $release_date="";
    public $medicine_bill="";
    public $room_bill="";
    public $doctor_bill="";
    public $prescription="";
    public $drugswithouthtml="";
    public $search="";
    public $filterByName="";
    public $filterByRoom="";

    public function __construct()
    {
        parent::__construct();
    }


    public function prepare($data=Array()){
        if(array_key_exists('patient_id',$data)) {
            $this->patient_id = $data['patient_id'];
            //echo $this->patient_id;
            //die();
        }
        if(array_key_exists('patient_name',$data)) {
            $this->patient_name = $data['patient_name'];
        }
        if(array_key_exists('mobile',$data)) {
            $this->mobile = $data['mobile'];
        }
        if(array_key_exists('disease',$data)) {
            $this->disease= $data['disease'];
        }
        if(array_key_exists('room_id',$data)) {
            $this->room_id = $data['room_id'];
        }
        if(array_key_exists('seat',$data)) {
            $this->seat = $data['seat'];
        }
        if(array_key_exists('entry_date',$data)) {
            $this->entry_date = $data['entry_date'];
        }
        if(array_key_exists('doctor_id',$data)) {
            $this->doctor_id = $data['doctor_id'];
        }
        if(array_key_exists('gender',$data)) {
            $this->gender = $data['gender'];
        }
        if(array_key_exists('age',$data)) {
            $this->age = $data['age'];
        }
        if(array_key_exists('release_date',$data)) {
            $this->release_date = $data['release_date'];
        }
        if(array_key_exists('medicine_bill',$data)) {
            $this->medicine_bill = $data['medicine_bill'];
        }
        if(array_key_exists('room_bill',$data)) {
            $this->room_bill = $data['room_bill'];
        }
        if(array_key_exists('doctor_bill',$data)) {
            $this->doctor_bill = $data['doctor_bill'];
        }
        if(array_key_exists('prescription',$data)) {
            $this->prescription = $data['prescription'];
        }
        if(array_key_exists('prescription',$data)) {
            $this->drugswithouthtml = strip_tags($data['prescription']);
        }
        if(array_key_exists('search',$data)) {
            $this->search= $data['search'];
        }
        if(array_key_exists('filterByName',$data)) {
            $this->filterByName= $data['filterByName'];
        }
        if(array_key_exists('filterByRoom',$data)) {
            $this->filterByRoom= $data['filterByRoom'];
        }
        return $this;

    }

    public function store(){
        $query1="INSERT INTO `hms`.`patient` (`patient_name`, `mobile`, `disease`,`room_id`,`seat`,`entry_date`,`doctor_id`,`age`,`gender`) VALUES ('".$this->patient_name."', '".$this->mobile."', '".$this->disease."', '".$this->room_id."','".$this->seat."', '".$this->entry_date."', '".$this->doctor_id."', '".$this->age."', '".$this->gender."')";
//echo $query1;
        //die();
        $result1= mysqli_query($this->conn,$query1);
        $query2="UPDATE `hms`.`room` SET `is_empty` = '1' WHERE `room`.`room_id` =".$this->room_id;
        $result2= mysqli_query($this->conn,$query2);
        if($result1){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You are successfully stored.
</div>");
            Utility::redirect('welcome.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has been stored Patient Data successfully.
</div>");
            Utility::redirect('welcome.php');

        }

    }
    public function store_prescription(){
        $query1="INSERT INTO `hms`.`prescription` (`drugs`,`drugswithouthtml`,`patient_id`) VALUES ('".$this->prescription."','".$this->drugswithouthtml."','".$this->patient_id."')";
echo $query1;
        //die();
        $result1= mysqli_query($this->conn,$query1);
        if($result1){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You are successfully stored.
</div>");
            Utility::redirect('patient_info.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has been stored Patient Data successfully.
</div>");
            Utility::redirect('patient_info.php');

        }

    }
    public function release_store(){
        $query1="UPDATE `hms`.`patient` SET `release_date` = '{$this->release_date}',`medicine_bill` = '{$this->medicine_bill}',`doctor_bill` = '{$this->doctor_bill}',`room_bill` = '{$this->room_bill}',`is_active` = '1' WHERE `patient`.`patient_id` =".$this->patient_id;
//echo $query1;
        //die();
        $result1= mysqli_query($this->conn,$query1);
        $query2="UPDATE `hms`.`room` SET `is_empty` = NULL WHERE `room`.`room_id` =".$this->room_id;
        $result2= mysqli_query($this->conn,$query2);
        if($result1){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Released successfully.
</div>");
           // Utility::redirect('bill_info.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has not Released successfully.
</div>");
           // Utility::redirect('bill_info.php');

        }

    }



    public function view(){
        $query="SELECT * FROM `patient` WHERE `patient_id` =".$this->patient_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }
    public function view_prescription(){
        $query="SELECT * FROM `prescription` WHERE `patient_id` =".$this->patient_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function room_no(){
        $query="SELECT * FROM `room` WHERE `room_id` =".$this->room_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }


    public function update(){
        $query="UPDATE `hms`.`patient` SET `patient_id` = '".$this->patient_id."',`patient_name` = '".$this->patient_name."',`mobile` = '".$this->mobile."',`disease` = '".$this->disease."',`room_id` = '".$this->room_id."',`seat` = '".$this->seat."',`entry_date` = '".$this->entry_date."',`doctor_id` = '".$this->doctor_id."',`gender` = '".$this->gender."',`age` = '".$this->age."' WHERE `patient`.`patient_id` = ".$this->patient_id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('patient_info.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('patient_info.php');

        }

    }
    public function delete()
    {
        $query = "DELETE FROM `hms`.`patient` WHERE `patient`.`patient_id` =" . $this->patient_id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect('patient_info.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect('patient_info.php');


        }
    }
    public function deleteReleased()
    {
        $query = "DELETE FROM `hms`.`patient` WHERE `patient`.`patient_id` =" . $this->patient_id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect('released_patient.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect('released_patient.php');


        }
    }

    public  function index(){
        $_allItem=array();
        $whereClause= " 1=1 ";
        if(!empty($this->filterByName)) {
            $whereClause .= " AND patient_name LIKE '%".$this->filterByName."%'";
        }
        if(!empty($this->filterByRoom)){
            $whereClause .= " AND room LIKE '%".$this->filterByRoom."%'";
        }
        if(!empty($this->search)){
            $whereClause .= " AND patient_name LIKE '%".$this->search."%'";
        }
        $query= "SELECT * FROM `patient` WHERE".$whereClause;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;
    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `patient`WHERE `is_active`IS NULL";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allItem=array();
        $query="SELECT * FROM `patient`WHERE `is_active`IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;

    }
    public function release_count(){
        $query="SELECT COUNT(*) AS totalItem FROM `patient`WHERE `is_active`IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function release_paginator($pageStartFrom=0,$Limit=5){
        $_allItem=array();
        $query="SELECT * FROM `patient`WHERE `is_active`IS NOT NULL LIMIT ".$pageStartFrom.",".$Limit;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;

    }
    public function allName(){
        $_allItem= array();
        $query="SELECT patient_name FROM `patient`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row['patient_name'];
        }
        return $_allItem;
    }
    public function allRoom(){
        $_allItem= array();
        $query="SELECT room_id FROM `patient`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allItem[]=$row['room_id'];
        }
        return $_allItem;
    }

}


