<?php

namespace App\User;
//session_start();
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
class User extends DB{
    public $id;
    public $first_name="";
    public $last_name="";
    public $email="";
    public $password="";

    public function __construct()
    {
        parent::__construct();
    }


    public function prepare($data=Array()){
    if(array_key_exists('first_name',$data)) {
        $this->first_name = $data['first_name'];
    }
    if(array_key_exists('last_name',$data)) {
        $this->last_name = $data['last_name'];
    }
    if(array_key_exists('email',$data)) {
        $this->email = $data['email'];
    }
    if(array_key_exists('password',$data)) {
        $this->password= md5($data['password']);
    }
    if(array_key_exists('id',$data)) {
        $this->id = $data['id'];
    }
        return $this;

    }

    public function store(){
        $query="INSERT INTO `hms`.`receptionist` (`first_name`, `last_name`, `email`, `password`) VALUES ('".$this->first_name."', '".$this->last_name."', '".$this->email."', '".$this->password."')";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You are successfully registered. You can log in now
</div>");
            Utility::redirect('../../index.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('../../index.php');

        }

    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `receptionist`";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allItem=array();
        $query="SELECT * FROM `receptionist` LIMIT ".$pageStartFrom.",".$Limit;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allItem[]=$row;
        }
        return $_allItem;

    }
    public function block(){
        $query="UPDATE `hms`.`receptionist` SET `is_block` =1 WHERE `receptionist`.`receptionist_id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Blocked successfully.
</div>");
            Utility::redirect('receptionist_info.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been Blocked successfully.
</div>");
            Utility::redirect('receptionist_info.php');


        }


    }
    public function Unblock(){
        $query="UPDATE `hms`.`receptionist` SET `is_block` =NULL WHERE `receptionist`.`receptionist_id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Unblocked successfully.
</div>");
            Utility::redirect('receptionist_info.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been Unblocked successfully.
</div>");
            Utility::redirect('receptionist_info.php');


        }


    }


}


