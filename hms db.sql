-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 17, 2016 at 05:27 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'sharadmohajan@gmail.com', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(255) NOT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_id`, `dept_name`) VALUES
(1, 'cardiology'),
(2, 'neurology'),
(3, 'orthopedix');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
  `doctor_id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dept_id` int(255) NOT NULL,
  PRIMARY KEY (`doctor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doctor_id`, `doctor_name`, `mobile`, `email`, `dept_id`) VALUES
(4, 'anwar', '01819785928', 'sharadmohajan305@gmail.com', 2),
(5, 'sharad', '01627837641', 'sharadmohajan30oi@gmail.com', 2),
(6, 'tanvir', '01829757110', 'sharadmohajan08@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menus` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menus`, `link`) VALUES
(1, 'Doctor Info', 'doctor_info.php'),
(2, 'InPatient', 'patient_info.php'),
(3, 'Released Patient', 'released_patient.php'),
(4, 'Profile', 'view_profile.php'),
(5, 'Log Out', 'Authenticate/logout.php');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
  `patient_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `disease` varchar(255) NOT NULL,
  `room_id` int(11) NOT NULL,
  `seat` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `doctor_id` int(255) NOT NULL,
  `release_date` date NOT NULL,
  `medicine_bill` int(11) NOT NULL,
  `doctor_bill` int(11) NOT NULL,
  `room_bill` int(11) NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `gender` text NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patient_id`, `patient_name`, `mobile`, `disease`, `room_id`, `seat`, `entry_date`, `doctor_id`, `release_date`, `medicine_bill`, `doctor_bill`, `room_bill`, `is_active`, `gender`, `age`) VALUES
(3, 'anwar ullah', '0183674652', 'hand pain', 4, 3, '2016-02-05', 5, '2016-09-01', 600, 100, 200, 1, 'female', 23),
(4, 'tanvir', '01627837641', 'headache', 1, 2, '2016-08-01', 6, '2016-09-01', 1000, 1000, 100, 1, 'female', 21),
(5, 'cv', '01819785928', 'df', 4, 1, '2016-08-01', 4, '2016-09-01', 500, 500, 500, 1, 'female', 23),
(6, 'sujit', '01823380483', 'fever', 5, 1, '2016-08-01', 5, '0000-00-00', 0, 0, 0, NULL, 'others', 25),
(7, 'anwar', '01829757110', 'headache', 4, 1, '2016-08-01', 5, '2016-09-01', 600, 200, 100, 1, 'female', 21),
(10, 'jahid', '5654657', 'dggh', 1, 1, '0000-00-00', 4, '0000-00-00', 0, 0, 0, NULL, '', 25),
(13, 'kaki', '01819785928', 'scean', 3, 1, '2016-08-01', 6, '2016-09-01', 500, 1000, 600, 1, 'female', 23),
(15, 'hasim', '01819785928', 'heart break', 4, 0, '2016-08-01', 5, '2016-09-01', 100, 500, 200, 1, 'male', 25),
(16, 'rubel', '01627837641', 'fever', 4, 0, '2016-08-01', 4, '2016-09-01', 600, 500, 100, 1, 'male', 30);

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE IF NOT EXISTS `prescription` (
  `prescription_id` int(11) NOT NULL AUTO_INCREMENT,
  `drugs` varchar(1024) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `drugswithouthtml` varchar(1024) NOT NULL,
  PRIMARY KEY (`prescription_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`prescription_id`, `drugs`, `patient_id`, `drugswithouthtml`) VALUES
(9, '<p>Morning:</p>\r\n<p>1.para</p>\r\n<p>Noon:</p>\r\n<p>1.ors</p>', 6, 'Morning:\r\n1.para\r\nNoon:\r\n1.ors'),
(10, '<p><strong>Morning:</strong></p>\r\n<p><strong>1.para</strong></p>\r\n<p><strong>2.orc</strong></p>', 12, 'Morning:\r\n1.para\r\n2.orc'),
(11, '<p><strong>Morning:</strong></p>\r\n<p><strong>Noon:</strong></p>\r\n<p><strong>Evening:</strong></p>', 10, 'Morning:\r\nNoon:\r\nEvening:'),
(12, '<p>morning:</p>\r\n<p>habijabi</p>\r\n<p>Noon:</p>\r\n<p>habijabi</p>\r\n<p>evening:</p>\r\n<p>habijabi</p>', 13, 'morning:\r\nhabijabi\r\nNoon:\r\nhabijabi\r\nevening:\r\nhabijabi'),
(13, '<p><strong>habijai</strong></p>', 15, 'habijai');

-- --------------------------------------------------------

--
-- Table structure for table `receptionist`
--

CREATE TABLE IF NOT EXISTS `receptionist` (
  `receptionist_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_block` int(11) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `shift` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`receptionist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `receptionist`
--

INSERT INTO `receptionist` (`receptionist_id`, `first_name`, `last_name`, `email`, `password`, `is_block`, `full_name`, `image`, `shift`, `mobile`, `age`) VALUES
(1, 'sharad', 'mohajan', 'sharadmohajan@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, 'sharad Mohajan', '', 'morning', '01829757110', 23),
(2, 'anwar', 'hossen', 'sharad@gmail.com', 'c99d31729b10fea4cfda6a5a92fc43da', NULL, 'Anwar ullah', '', 'night', '01829757110', 25),
(3, 'tanvir', 'hossen', 'sharad1@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(4, 'tanu', 'hossen', 'sharad2@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(5, 'fanu', 'hossen', 'sharad3@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(6, 'tonu', 'jh', 'sharad4@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', NULL, '', '', '', '', 0),
(7, 'nayeem', 'uddin', 'nayeem1@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 'nayeem moklech', '14714089671185520_566120206788571_566424367_n.jpg', 'night', '01829757110', 25);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_no` int(11) NOT NULL,
  `is_empty` int(11) DEFAULT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`room_id`, `room_no`, `is_empty`) VALUES
(1, 101, 1),
(2, 102, 1),
(3, 103, 1),
(4, 104, NULL),
(5, 105, 1),
(6, 107, NULL);
